from django.urls import path
from todos.views import todo_list, show_todo_item, create_todo_list


urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_todo_item, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
]
